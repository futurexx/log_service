### Server startup
```bash
pip install -r requirements.pip
cd log_service
export SECRET_KEY='some-secret-key'
python manage.py runserver
```
It will install requirements and running server on localhost:8000

### Generate log data
```bash
python gen_logfile.py 
```
This will generate a 1 GB log file.

### Usage
After running server you can try to send request.
* via curl:
  ```bash
  curl -X POST -H "Content-Type: application/json" -d '{"offset": 0}' http://localhost:8000/api/read_log
  ```
* via python:
  ```python
  import requests

  url = 'http://localhost:8000/api/read_log'
  data = {'offset': 0}
  response = requests.post(url, json=data)
  print response.json()
  ```
