
import time
import random


STATUS_CHOICES = ("DEBUG", "INFO",
                  "WARN", "ERROR")

FILE_SIZE = 1024 * 1024 * 1024  # 1 GB


def get_log_record(counter):
    return u'{"level": "%s", "message": "Test message %s"}\n' % \
            (random.choice(STATUS_CHOICES), str(counter))


if __name__ == '__main__':
    with open('log_service/output.log', 'w') as log_file:
        counter = 1
        while log_file.tell() < FILE_SIZE:
            record = get_log_record(counter)
            log_file.write(record)
            counter += 1
