# -*- coding: utf-8 -*-

from rest_framework.views import APIView

from .helpers import *


class ReadLogView(APIView):
    """
        /api/read_log view
        :methods POST
        :param offset: int
    """
    @view_handler
    def post(self, request, *args, **kwargs):
        check_logfile()

        offset = request.data.get('offset')
        check_offset(offset)

        with open(settings.LOG_FILE, 'r') as log_file:
            log_file.seek(0, 2)
            total_size = log_file.tell()
            log_file.seek(offset)

            messages = get_log_messages(log_file)

            data = {
                'total_size': total_size,
                'next_offset': log_file.tell(),
                'messages': messages
            }
            return data
