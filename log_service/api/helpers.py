# -*- coding: utf-8 -*-

import os
import json

from django.conf import settings

from rest_framework.response import Response

from .exceptions import APIException


def view_handler(action):
    def wrapper(*args, **kwargs):
        try:
            data = action(*args, **kwargs)
            data['ok'] = True

        except APIException as e:
            data = {
                'ok': False,
                'reason': e.message
            }
        finally:
            return Response(data)
    return wrapper


def check_offset(offset):
    if offset is None:
        raise APIException('`offset` is required')
    if not isinstance(offset, int) or offset < 0:
        raise APIException('`offset` must be a positive number')


def check_logfile():
    if not os.path.isfile(settings.LOG_FILE):
        raise APIException('log file was not found')


def get_log_messages(log_file):
    messages = []
    for _ in range(settings.PAGE_SIZE):
        line = log_file.readline()
        if line == '':
            break
        message = json.loads(line.strip())
        messages.append(message)
    return messages
